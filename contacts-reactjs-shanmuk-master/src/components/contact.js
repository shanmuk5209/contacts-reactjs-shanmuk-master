import React, { Component } from 'react';
// import * as opertaions from './operations';
// import EditModal from './EditModal'
import {Button} from 'react-bootstrap'
class Contact extends Component {
    state = {
        contact: this.props.data,
        delete: this.props.DeleteFun,
        edit: this.props.EditFun,
        // handleShow:this.props.handleShow
    }
    render() {
        return (<tr key={this.state.contact.id} id={this.state.contact.id}>
            <td className="align-middle"> {this.state.contact.id} </td>
            <td className="align-middle"> <img src={this.state.contact.avatar_url} alt='This is my dp'></img> </td>
            <td className="align-middle"><span>{this.state.contact.first_name} </span>  <span> {this.state.contact.last_name}</span></td>
            <td className="align-middle">  {this.state.contact.phone}</td>
            <td className="align-middle">
            <Button variant="primary" id={this.state.contact.id} onClick={this.state.edit}>
              Edit
            </Button>
         <button type="button" id={this.state.contact.id} onClick={this.state.delete} className="btn btn-danger"> delete</button>
            </td>
        </tr>);
    }
}

export default Contact;