import React, { Component,Fragment} from 'react';
import { Button,Modal,Form,Col } from 'react-bootstrap';

class CreateModal extends Component {
    constructor(props)
    {
        super(props)
        this.state={
            handleClose:this.props.handleClose,
            submit:this.props.submit,
            show:this.props.show   }
    }
    render() { 
        return (<Fragment>
                <Modal show={this.state.show}  onHide={this.state.handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Edit  Contact</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                    <Form  onSubmit={this.state.submit}>
                    <Form.Row>
                      <Form.Group as={Col} controlId="inputFirstName">
                        <Form.Label>FirstNam</Form.Label>
                        <Form.Control type="text"  placeholder="First Name"/>
                      </Form.Group>
                  
                      <Form.Group as={Col} controlId="inputLastName">
                        <Form.Label>LastName</Form.Label>
                        <Form.Control type="text" placeholder="Last Name" />
                      </Form.Group>
                    </Form.Row>
                    <Form.Row>
                    <Form.Group as={Col} controlId="inputEmailAddress">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="text" placeholder="Email Address"/>
                      </Form.Group>
                    
                      <Form.Group as={Col} controlId="inputPhoneNumber">
                        <Form.Label>Phone Number</Form.Label>
                        <Form.Control type="text" placeholder="Phone Number" />
                      </Form.Group>
                    </Form.Row>
                    <Button variant="primary" type="submit"> Submit</Button>
                  </Form>
                       
              </Modal.Body>
              
            </Modal>
            </Fragment>);
    }
}
 
export default CreateModal;