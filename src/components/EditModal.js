import React, { Component } from 'react';
import { Button,Modal,Form,Col } from 'react-bootstrap';



class EditModal extends Component {
    constructor(props)
    {
        super(props)
        this.state={
            handleClose:this.props.handleClose,
            contact:this.props.data,
            submit:this.props.submit,
            show:this.props.show   }
    }
    render() { 
        return ( <>
            
            <Modal show={this.state.show}  onHide={this.state.handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Edit {this.state.contact.map(e=>e.first_name)} Contact</Modal.Title>
              </Modal.Header>
              <Modal.Body>
              {Object.values(this.state.contact).map(e=>
                    <Form key={e.id} id={e.id} onSubmit={this.state.submit}>
                    <Form.Row>
                      <Form.Group as={Col} controlId="inputFirstName">
                        <Form.Label>FirstNam</Form.Label>
                        <Form.Control type="text" defaultValue={e.first_name} />
                      </Form.Group>
                  
                      <Form.Group as={Col} controlId="inputLastName">
                        <Form.Label>LastName</Form.Label>
                        <Form.Control type="text" defaultValue={e.last_name} />
                      </Form.Group>
                    </Form.Row>
                    <Form.Row>
                    <Form.Group as={Col} controlId="inputEmailAddress">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="text" defaultValue={e.email}/>
                      </Form.Group>
                    
                      <Form.Group as={Col} controlId="inputPhoneNumber">
                        <Form.Label>Phone Number</Form.Label>
                        <Form.Control type="text" defaultValue={e.phone} />
                      </Form.Group>
                    </Form.Row>
                    <Button variant="primary" type="submit"> Submit</Button>
                  </Form>
                )} 
                       
              </Modal.Body>
              
            </Modal>
          </> );
    }
}
 
export default EditModal;
